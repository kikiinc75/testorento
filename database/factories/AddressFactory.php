<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    $users = App\User::pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($users),
        'detail' => $faker->address,
        'preferred' => $faker->boolean(1),
    ];
});
