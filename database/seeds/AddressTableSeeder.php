<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Address;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Address::class, 10)->create();
        $faker = Faker\Factory::create();

        $user = App\User::pluck('id')->toArray();
        for ($i = 0; $i < 10; $i++) {
            $userNew = Address::where('user_id', $user[$i])->where('preferred', 1)->count();
            for ($a = 1; $a <= count($user); $a++) {
                if ($userNew == 1) {
                    Address::create([
                        'user_id' => $a,
                        'detail' => $faker->address,
                        'preferred' => 0
                    ]);
                } else {
                    Address::create([
                        'user_id' => $a,
                        'detail' => $faker->address,
                        'preferred' => 1
                    ]);
                }
            }
        }
    }
}
