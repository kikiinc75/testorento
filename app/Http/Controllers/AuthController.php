<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use JWTAuthException;
use App\Address;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => 'show']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5',
            'address' => 'required',
        ]);

        $username = $request->input('username');
        $email = $request->input('email');
        $password = $request->input('password');
        $address = $request->input('address');

        $user = new User([
            'username' => $username,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        $cridentials = [
            'email' => $email,
            'password' => $password
        ];

        if ($user->save()) {

            $token = null;
            try {
                if (!$token = JWTAuth::attempt($cridentials)) {
                    return response()->json([
                        'msg' => 'Email or Password are incorrect'
                    ], 404);
                }
            } catch (JWTAuthException $e) {
                return response()->json([
                    'msg' => 'Failed_to_create_token'
                ], 404);
            }
            $addresses = new Address;
            $addresses->user_id = $user->id;
            $addresses->detail = $address;
            $addresses->preferred = 1;
            $addresses->status = 'active';

            $addresses->save();

            $user->addresses = [
                'href' => 'api/user/' . $user->id,
                'method' => 'GET',
            ];
            $response = [
                'msg' => 'User Created',
                'user' => $user,
                'address' => $addresses,
                'token' => $token
            ];
            return response()->json($response, 201);
        }
    }

    public function show($id)
    {
        $address = Address::where('status', 'active')->where('user_id', $id)->paginate(3);
        $user = User::findOrFail($id);
        $response = [
            'msg' => 'address information',
            'user' => $user,
            'address' => $address
        ];
        return response()->json($response, 200);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:5'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');


        if ($user = User::where('email', $email)->first()) {
            $cridentials = [
                'email' => $email,
                'password' => $password
            ];

            $token = null;
            try {
                if (!$token = JWTAuth::attempt($cridentials)) {
                    return response()->json([
                        'msg' => 'Email or Password are incorrect'
                    ], 404);
                }
            } catch (JWTAuthException $e) {
                return response()->json([
                    'msg' => 'Failed_to_create_token'
                ], 404);
            }

            $response = [
                'msg' => 'User signin',
                'user' => $user,
                'token' => $token
            ];
            return response()->json($response, 201);
        }

        $response = [
            'msg' => 'An error occurred'
        ];

        return response()->json($response, 404);
    }
}
