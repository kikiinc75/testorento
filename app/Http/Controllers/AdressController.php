<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\User;

class AdressController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'address' => 'required',
            'preferred' => 'required|boolean',
        ]);

        $user_id = $request->input('user_id');
        $address = $request->input('address');
        $preferred = $request->input('preferred');

        $user = User::findOrFail($user_id);
        if ($user) {
            if ($preferred == 1) {
                $oldAddress = Address::where('user_id', $user_id)->where('preferred', 1)->first();
                if ($oldAddress) {
                    $oldAddress->preferred = 0;
                    $oldAddress->save();
                }
            }
            $addresses = new Address([
                'user_id' => $user_id,
                'detail' => $address,
                'preferred' => $preferred,
            ]);
        }

        if ($addresses->save()) {
            // $addresses->users()->attach($user_id);
            $addresses->view_addresses = [
                'href' => 'api/user/' . $addresses->user_id,
                'method' => 'GET'
            ];
            $message = [
                'msg' => 'addresses Created',
                'addresses' => $addresses
            ];
            return response()->json($message, 201);
        }

        $response = [
            'msg' => 'Error during creation',
        ];

        return response()->json($response, 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $address = Address::findOrFail($id);
        $user = User::find($address->user_id);
        $response = [
            'msg' => 'address information',
            'user' => $user,
            'address' => $address,
        ];
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
            'address' => 'required',
            'preferred' => 'required|boolean',
        ]);

        $user_id = $request->input('user_id');
        $address = $request->input('address');
        $preferred = $request->input('preferred');

        $addresses = Address::findOrFail($id);

        if (!$addresses->where('user_id', $user_id)->first()) {
            return response()->json(['msg' => 'user not registered for addresses, Update not successful'], 401);
        }

        if ($preferred == 1) {
            $oldAddress = Address::where('user_id', $user_id)->where('preferred', 1)->first();
            if ($oldAddress) {
                $oldAddress->preferred = 0;
                $oldAddress->save();
            }
        }

        $addresses->detail = $address;
        $addresses->preferred = $preferred;

        if (!$addresses->update()) {
            return response()->json([
                'msg' => 'Error during update'
            ], 404);
        }

        $addresses->view_addresses = [
            'href' => 'api/address/' . $addresses->id,
            'method' => 'GET'
        ];

        $response = [
            'msg' => 'address Updated',
            'address' => $addresses
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Address::findOrFail($id);

        $address->status = 'archived';
        if (!$address->update()) {
            return response()->json([
                'msg' => 'Deletion Failed'
            ], 404);
        }

        $response = [
            'msg' => 'address deleted',
            'create' => [
                'href' => 'api/address',
                'method' => 'POST',
                'params' => 'address, user_id, preferred'
            ]
        ];

        return response()->json($response, 200);
    }
}
