<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => ['cors']], function () {
    Route::resource('address', 'AdressController', [
        'except' => ['create', 'edit', 'index']
    ]);

    Route::post('/user/register', 'AuthController@store');
    Route::post('/user/login', 'AuthController@login');
    Route::get('/user/{id}', 'AuthController@show');
});
